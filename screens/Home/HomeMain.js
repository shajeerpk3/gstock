import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import Header from '../MainScreens/Header';
import Layout from '../../constants/Layout';
import Search from '../MainScreens/Search';
import Banner from '../MainScreens/Banner';
import CategoriesHorizontal from './CategoriesHorizontalRound'

export default class Homemain extends React.Component {

    static navigationOptions = ({ title, subtitle }) => {
        return {
            headerStyle: {
                height: Layout.headerHeight,
                width: Layout.window.width,
                borderBottomWidth: 0,
            },
            headerTitle: <Header />,
        };
    };


    

    render() {
        return (
            <ScrollView style={styles.container}>
                {/* Go ahead and delete ExpoLinksView and replace it with your
           * content, we just wanted to provide you with some helpful links */}
           <Search></Search>
           <Banner></Banner>
           <CategoriesHorizontal></CategoriesHorizontal>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
       
        backgroundColor: '#fff',
    },
});
