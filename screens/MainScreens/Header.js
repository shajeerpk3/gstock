import React from 'react';
import { TouchableOpacity, ScrollView, StyleSheet, View, Text,Image } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';
import { createIconSetFromIcoMoon, createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../src/config.json';
import icoMoonConfig from '../../src/selection.json';


const Icon = createIconSetFromFontello(fontelloConfig);
const Icon2 = createIconSetFromIcoMoon(icoMoonConfig);
export default class Header extends React.Component {

    render() {
        return (
            <View style={styless.header}>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' ,paddingLeft:5,paddingRight:5}}>
                    <View style={{ height: Layout.headerHeight, justifyContent:'center'}}><TouchableOpacity><Icon2 name="Menu" size={25} style={{ fontWeight: 5 }} /></TouchableOpacity></View>
                    <View style={{ height: Layout.headerHeight, justifyContent:'center' }}><Image style={{height: Layout.headerHeight-5,width:Layout.window.width/3 +50}} resizeMode="contain" source={require('../../assets/images/logo.png')} /></View>
                    <View style={{ height: Layout.headerHeight, justifyContent:'center' }}><TouchableOpacity><Icon2 name="Product-alert" size={25} style={{ fontWeight: 5 }} /></TouchableOpacity></View>
                </View>

            </View>
        );
    }



}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});

const styless = StyleSheet.create({
    header: {
        backgroundColor: Colors.headerColor,
        flex: 1,
        alignSelf: 'stretch',
        height: '100%',
        width:Layout.window.width

    },
    title: {
        fontSize: 20,
        color: 'blue',
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: 16,
        color: 'purple',
        fontWeight: 'bold',
    },
});
